//
//  APIConstants.swift
//  MercadoLibre
//
//  Created by Felipe Sprovieri on 07/04/19.
//

import Foundation

struct APIConstants {
    static let urlSearchProduct = "https://api.mercadolibre.com/sites/MLU/search?q="
}
