//
//  MLProduct.swift
//  MercadoLibre
//
//  Created by Felipe Sprovieri.
//

import Foundation

class MLProduct {
    var id: String = ""
    var title: String = ""
    var price: Double = 0.0
    var installments: MLProductInstallments = MLProductInstallments()
    var currencyId: String = ""
    var availableQuantity: Int = 0
    var soldQuantity: Int = 0
    var condition: String = ""
    var permaLink: String = ""
    var thumbNail: String = ""
    var arrReviews: [MLProductReview] = []
    
    init(json: [String: Any]) throws {
        if let id = json["id"] as? String {
            self.id = id
        }
        if let title = json["title"] as? String {
            self.title = title
        }
        if let price = json["price"] as? Double {
            self.price = price
        }
        
        if let installments = json["installments"] as? [String: Any] {
            let mlProductInstallments = MLProductInstallments()
            if let quantity = installments["quantity"] as? Int {
                mlProductInstallments.quantity = quantity
            }
            if let amount = installments["amount"] as? Double {
                mlProductInstallments.amount = amount
            }
            if let rate = installments["rate"] as? Int {
                mlProductInstallments.rate = rate
            }
            if let currencyId = installments["currency_id"] as? String {
                mlProductInstallments.currencyId = currencyId
            }
            
            self.installments = mlProductInstallments
        }
        if let currencyId = json["currency_id"] as? String {
            self.currencyId = currencyId
        }
        if let availableQuantity = json["available_quantity"] as? Int {
            self.availableQuantity = availableQuantity
        }
        if let soldQtde = json["sold_quantity"] as? Int {
            self.soldQuantity = soldQtde
        }
        if let condition = json["condition"] as? String {
            self.condition = condition
        }
        if let permaLink = json["permalink"] as? String {
            self.permaLink = permaLink
        }
        if let thumbNail = json["thumbnail"] as? String {
            self.thumbNail = thumbNail
        }
        if let reviews = json["reviews"] as? [[String: Any]] {
            for review in reviews {
                let productReview = MLProductReview()
                
                if let rating = review["rating_average"] as? Double {
                    productReview.ratingAverage = rating
                }
                if let totalRatings = review["total"] as? Int {
                    productReview.totalRatings = totalRatings
                }
                
                self.arrReviews.append(productReview)
            }
        }
    }
}

class MLProductInstallments {
    var quantity: Int = 0
    var amount: Double = 0.0
    var rate: Int = 0
    var currencyId: String = ""
}

class MLProductReview {
    var ratingAverage: Double = 0.0
    var totalRatings: Int = 0
}
