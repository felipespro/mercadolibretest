//
//  MLProductResultTableViewCell.swift
//  MercadoLibre
//
//  Created by Felipe Sprovieri.
//

import UIKit
import Kingfisher

class MLProductResultTableViewCell: UITableViewCell {
    static let identifier = "productResultCell"
    @IBOutlet weak var imgViewProduct: UIImageView!
    @IBOutlet weak var lblProductDescription: UILabel!
    @IBOutlet weak var lblProductFullPrice: UILabel!
    @IBOutlet weak var lblProductInstallments: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupCell(product: MLProduct) {
        let imgURL = URL(string: product.thumbNail)
        imgViewProduct.kf.setImage(with: imgURL)
        
        lblProductDescription.text = product.title
        lblProductFullPrice.text = product.price.toStringDollars()
        lblProductInstallments.text = "\(product.installments.quantity)x \(product.installments.amount.toStringDollars())"
    }
    
}
