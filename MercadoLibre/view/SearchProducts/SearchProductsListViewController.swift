//
//  SearchProductsListViewController.swift
//  MercadoLibre
//
//  Created by Felipe Sprovieri.
//

import UIKit

//MARK: -SEARCH PRODUCTS LIST INTERFACES

protocol SearchProductsDelegate {
    func showLoading()
    func hideLoading()
    func searchProducts(searchText: String)
    func showDetail(product: MLProduct)
}

//MARK: -VIEW CONTROLLER

class SearchProductsListViewController: UIViewController {
    
    //MARK: -PROPERTIES
    let presenter = SearchProductsListPresenter()
    var arrProducts: [MLProduct] = []
    var selectedProductDetail: MLProduct?
    
    //MARK: -IBOUTLETS
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewLoading: UIView!
    
    //MARK: -LIFECYCLE METHODS

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.addView(view: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier! == ProductDetailViewController.identifier) {
            let vc = segue.destination as! ProductDetailViewController
            if(self.selectedProductDetail != nil) {
                vc.product = self.selectedProductDetail
            }
        }
    }

}

//MARK: -TABLEVIEW METHODS

extension SearchProductsListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let product = arrProducts[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: MLProductResultTableViewCell.identifier) as! MLProductResultTableViewCell
        
        cell.setupCell(product: product)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let product = self.arrProducts[indexPath.row]
        showDetail(product: product)
    }
}

//MARK: -SEARCHBAR METHODS

extension SearchProductsListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("Cancelou busca")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchProducts(searchText: searchBar.text!)
    }
}

//MARK: -INTERFACES IMPLEMENTATION

extension SearchProductsListViewController: SearchProductsDelegate {
    func showLoading() {
        self.viewLoading.isHidden = false
    }
    
    func hideLoading() {
        self.viewLoading.isHidden = true
    }
    
    func searchProducts(searchText: String) {
        presenter.searchProduct(searchText: searchText)
    }
    
    func showDetail(product: MLProduct) {
        self.selectedProductDetail = product
        self.performSegue(withIdentifier: ProductDetailViewController.identifier, sender: self)
    }
}
