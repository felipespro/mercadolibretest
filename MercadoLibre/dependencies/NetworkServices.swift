//
//  NetworkServices.swift
//  MercadoLibre
//
//  Created by Felipe Sprovieri.
//

import Foundation
import Alamofire

class NetworkServices {
    
    static func isConnectToInternet() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
    
    func doGETRequest(url: String, onSuccess: @escaping (_ arrMLProducts: [MLProduct]) -> Void, onError: @escaping (_ error: ServiceError) -> Void) {
        AF.request(url).responseJSON { response in
            print("Request: \(String(describing: response.request))")
            // original url request
            print("Response: \(String(describing: response.response))")
            // http url response
            print("Result: \(response.result)")
            // response serialization result

            if let json = response.result.value as? [String: Any] {
                print("JSON: \(json)") // serialized json response
                var arrProducts: [MLProduct] = []
                if let results = json["results"] as? [[String: Any]] {
                    for obj in results  {
                        do {
                            let product = try MLProduct.init(json: obj)
                            arrProducts.append(product)
                        }catch {
                            print(ServiceError.parseError)
                            onError(ServiceError.genericError)
                        }
                    }
                    onSuccess(arrProducts)
                }else {
                    onError(ServiceError.genericError)
                }
            }

            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)") // original server data as UTF8 string
            }
        }
    }
}
