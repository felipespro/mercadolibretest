//
//  UIExtensions.swift
//  MercadoLibre
//
//  Created by Felipe Sprovieri.
//

import UIKit

extension UIViewController {
    
    func showAlert(title: String, description: String) {
        let alertController = UIAlertController(
            title: title,
            message: description,
            preferredStyle: .alert
        )
        
        let dismissAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(dismissAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
