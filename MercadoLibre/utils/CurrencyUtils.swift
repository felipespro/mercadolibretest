//
//  CurrencyUtils.swift
//  MercadoLibre
//
//  Created by Felipe Sprovieri.

import Foundation

extension Double {
    func toStringDollars() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "en-US")
        
        var valor = ""
        
        if let str = formatter.string(from: NSNumber(value: self)) {
            valor = str
        }
        
        return valor.replacingOccurrences(of: "U$", with: "U$ ")
    }
}
