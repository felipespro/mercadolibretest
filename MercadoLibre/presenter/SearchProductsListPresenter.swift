//
//  SearchProductsListPresenter.swift
//  MercadoLibre
//
//  Created by Felipe Sprovieri.
//

import Foundation

class SearchProductsListPresenter {
    var view: SearchProductsListViewController!
    let networkServices = NetworkServices()
    
    func addView(view: SearchProductsListViewController) {
        self.view = view
    }
    
    func searchProduct(searchText: String) {
        view.showLoading()
        
        let url = APIConstants.urlSearchProduct + searchText
        
        self.networkServices.doGETRequest(url: url, onSuccess: { (response) in
            //PARSE
            self.view.arrProducts = response
            self.view.tableView.reloadData()
            self.view.hideLoading()
        }) { (error) in
            self.view.hideLoading()
            self.view.showAlert(title: UIMessages.errorTitle, description: error.rawValue)
        }
    }
}
